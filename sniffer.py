#!/usr/bin/env python

# sniffer.py

# TODO: Add description
#
#

import argparse
import signal
import os
import sys
import threading
from binascii import hexlify
from serial import Serial
from time import time, sleep
from datetime import datetime
from libmich.formats.IEEE802154 import TI_USB, IEEE802154
from modules.Interpreter import INTERPRETER
from modules.CC2531 import *

# this is to customize another 802.15.4 frame decoder
DECODER = IEEE802154
# this is the default CC2531 behavior
DECODER.PHY_INCL = False
DECODER.FCS_INCL = False
# Global variables
RUNNING = True  # Indicates if program is still running
THREADS = []  # Contains list of threads
POSITION = {'Latitude': '',  # Contains position data
            'Longitude': ''}
global_lock = threading.Lock()


class TI_PARSER():
    def __init__(self):
        self.data = ''
        self.macDict = {}

    def parseUSB(self):
        self.macDict = {}

        self.usb = TI_USB()
        try:
            self.usb.map(self.data)
        except Exception as e:
            print(e)
            return
        # process only 802.15.4 frames with correct checksum,
        # or process all frames if FCS is ignored
        if self.usb.TI_CC.FCS():
            self.macDict['dev_ts'] = self.usb.TS()
            self.macDict['RSSI'] = self.usb.TI_CC.RSSI()
            self.macDict['frame'] = self.usb.TI_CC.Payload()
            self.macDict['FCS_OK'] = self.usb.TI_CC.FCS()
            mac = DECODER()
            try:
                mac.parse(self.macDict['frame'])
            except Exception as e:
                print(e)
                mac = ''
            self.macDict['payload'] = mac


class runCC2531(threading.Thread):
    def __init__(self, dongle, channel_list, args):
        threading.Thread.__init__(self)
        self.dongle = dongle
        self.channel_list = channel_list
        self.args = args  # Arguments from argument parser
        self.TIMEOUT = 1.5  # Channel scan time in seconds

    # This method reads data from a CC2531 dongle and parses the output
    def read_cc(self, cc, ti_parser, interpreter):
        global POSITION
        global global_lock

        # Start capturing
        cc.start_capture()
        # Fixate start of timer
        timeout_start = time()
        # Read data TIMEOUT seconds
        while time() < timeout_start + self.TIMEOUT:

            ti_parser.data = cc.read_data()

            # Only process data if data is present
            if ti_parser.data:
                # parse CC2531 data and store in dictionary macDict() of TI_PARSER class
                ti_parser.parseUSB()

                cur_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                try:
                    rawData = hexlify(ti_parser.macDict.get('frame'))
                except Exception as e:
                    rawData = None
                longitude = POSITION.get('Longitude')
                latitude = POSITION.get('Latitude')
                if rawData:
                    global_lock.acquire()
                    with open("output.csv", "a+") as file:
                        file.write(cur_time + "," + rawData + "," + longitude + "," + latitude + "\n")
                    global_lock.release()

                try:
                    # Only write to stdout if silent flag not set
                    if not self.args.silent and rawData:
                        interpreter.parse(ti_parser.macDict.get('frame'))
                        interpreter.show_Frame()
                except Exception as e:
                    print('Error while parsing frame\nMessage: %s\n' % e)
                    pass

    # This methos initializes and runs cc2531 dongles
    def run(self):
        cc = CC2531(self.dongle)
        cc.DEBUG = self.args.verbose
        cc.init()

        # Create instance of TI_PARSER() class
        ti_parser = TI_PARSER()

        # Create instance of INTERPRETER class
        interpreter = INTERPRETER()

        if self.args.channel:
            cur_channel = 0
            channel = self.channel_list[cur_channel]
            cc.config(channel)
            while RUNNING:
                self.read_cc(cc, ti_parser, interpreter)
        else:
            cur_channel = 0
            iter_channels = True
            if len(self.channel_list) == 1:
                iter_channels = False
                channel = self.channel_list[cur_channel]
                cc.config(channel)
            while RUNNING:
                if iter_channels:
                    channel = self.channel_list[cur_channel]
                    cc.stop_capture()
                    cc.config(channel)
                self.read_cc(cc, ti_parser, interpreter)
                if iter_channels:
                    cur_channel += 1
                    cur_channel = cur_channel % len(self.channel_list)

        cc.stop_capture()
        cc.close()


def arg_parser():
    # Argument parser to display usage information and handle arguments
    parser = argparse.ArgumentParser(description='Drives one or more CC2531 dogles and collects ZigBee Network information. This program must be run as root, otherwise it can\'t access connected CC2531 dongles (sudo python sniffer.py [options])')
    parser.add_argument('-s', '--silent', action='store_true', help='run in silent mode and don\'t write packet information to stdout')
    parser.add_argument('-v', '--verbose', type=int, default=0, choices=range(0, 4),
                        help='sets verbosity level to output additional debug information (0 = none, 3 = max, default = 0)')
    parser.add_argument('-p', '--position', action='store_true', help='print positional data to stdout')
    parser.add_argument('-c', '--channel', type=int, default=None, choices=range(11, 26),
                        help='define a single channel to scan')
    return parser.parse_args()


def startup(dongle_cnt, gps_connected):
    # Check if program is running as root
    if os.getuid() != 0:
        print('[STARTUP] Error - Program must run as root.')
        sys.exit(0)

    print('[STARTUP] ZIGSCANNER started')
    print('[STARTUP] Nr. of CC2531 dongles connected: %i' % dongle_cnt)

    if dongle_cnt == 0:
        print('[STARTUP] No CC2531 dongles found, exiting program')
        sys.exit(0)

    if gps_connected:
        print('[STARTUP] GPS connected')
    else:
        print('[STARTUP] No GPS connected')

    if not os.path.exists("output.csv"):
        with open("output.csv", "w+") as file:
            file.write('time,rawData,longitude,latitude\n')


def signal_handler(signal, frame):
    global THREADS
    global RUNNING
    print('\n[SIGINT] Exiting program, waiting for workers to finish.\n')
    for t in THREADS:
        t.running = False
    RUNNING = False


def gps_iterator(gps):
    while True:
        line = gps.readline()
        line = line.split(',')
        if line[0] == '$GPRMC':
            yield line


def get_channels(channels, nr_devices, args):
    channel_dict = {0: [],
                    1: [],
                    2: [],
                    3: [],
                    4: [],
                    5: [],
                    6: [],
                    7: [],
                    8: [],
                    9: [],
                    10: [],
                    11: [],
                    12: [],
                    13: [],
                    14: [],
                    15: []}

    if args.channel:
        for cur_list in range(nr_devices):
            channel_dict[cur_list].append(args.channel)

    elif nr_devices >= 4:
        channel_dict[0].append(0xF)
        del channels[0xF]
        channel_dict[1].append(0x14)
        del channels[0x14]
        channel_dict[2].append(0x19)
        channel_dict[2].append(0x1a)
        del channels[0x19]
        del channels[0x1a]

        cur_list = 3
        for channel in channels:
            channel_dict[cur_list].append(channel)
            cur_list += 1
            cur_list = cur_list % nr_devices
            if cur_list == 0:
                cur_list += 3

    else:
        cur_list = 0
        for channel in channels:
            channel_dict[cur_list].append(channel)
            cur_list += 1
            cur_list = cur_list % nr_devices

    return channel_dict


def main():
    # Global variables
    global POSITION
    global THREADS

    # Variables
    args = arg_parser()  # Contains arguments passed to sniffer.py
    dongle_list = get_CC2531()  # Contains list of connected CC2531 dongles
    gps_path = '/dev/ttyACM0'  # Path to GPS device
    gps_connected = os.path.exists(gps_path)

    # Execute startup procedure
    startup(len(dongle_list), gps_connected)

    # Catch and handle SIGINT signal
    signal.signal(signal.SIGINT, signal_handler)

    # Split channels by nr of awailable CC2531 dongles
    channel_dict = get_channels(CHANNELS, len(dongle_list), args)

    # Run CC2531 dongles and start capturing
    i = 0
    for dongle in dongle_list:
        th = runCC2531(dongle, channel_dict[i], args)
        th.start()
        THREADS.append(th)
        i += 1

    # If GPS device is connected update position indefinetly until SIGINT is caught
    # else run idefinetly until SIGINT is caught
    if gps_connected:
        gps = Serial(gps_path)
        for position in gps_iterator(gps):
            POSITION['Latitude'] = position[3]
            POSITION['Longitude'] = position[5]

            if args.position:
                print('[POSITION] Latitude=%s Longitude=%s' % (POSITION['Latitude'], POSITION['Longitude']))

            # If SIGINT close serial port and break loop
            if not RUNNING:
                gps.close()
                break
    else:
        while RUNNING:
            sleep(1)


if __name__ == "__main__":
    main()
