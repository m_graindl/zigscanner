#!/usr/bin/env python
# Interpreter.py

# Imports
from binascii import hexlify

# Export filtering
__all__ = ['INTERPRETER']

# Dictionaries zur Interpretation von diversen Flags und Werten
MAC_FRAME_TYPES = {0: 'Beacon',
                   1: 'Data',
                   2: 'ACK',
                   3: 'MAC Command',
                   4: 'Reserved',
                   5: 'Reserved',
                   6: 'Reserved',
                   7: 'Reserved'}

MAC_ADDRESSING_MODES = {0: 'None',
                        1: 'Reserved',
                        2: '16-Bit Address',
                        3: '64-Bit Address'}

BOOL_VALUES = {0: False,
               1: True}

NWK_FRAME_TYPES = {'00': 'Data',
                   '01': 'NWK command',
                   '10': 'Reserved',
                   '11': 'Inter-PAN'}

NWK_PROTOCOL_VERSIONS = {0: 'None',
                         1: 'ZigBee 2004',
                         2: 'ZigBee 2006, ZigBee Pro',
                         3: 'ZigBee Green Power'}

NWK_COMMAND_ID = {0: 'Reserved',
                  1: 'Route request',
                  2: 'Route reply',
                  3: 'Network status',
                  4: 'Leave',
                  5: 'Route record',
                  6: 'Rejoin request',
                  7: 'Rejoin response',
                  8: 'Link status',
                  9: 'Network report',
                  10: 'Network update',
                  11: 'End device timeout reqeust',
                  12: 'End device timeout response'}

DISCOVER_ROUTES = {0: 'Suppress route discovery',
                   1: 'Enable route discovery',
                   2: 'Reserved',
                   3: 'Reserved'}

MULTICAST_MODES = {0: 'Non-member mode',
                   1: 'Member mode',
                   2: 'Reserved',
                   3: 'Reserved'}

APL_FRAME_TYPES = {'00': 'Data',
                   '01': 'Command',
                   '10': 'ACK',
                   '11': 'Inter-PAN APS'}

APL_DELIVERY_MODES = {'00': 'Normal unicast',
                      '01': 'Reserved',
                      '10': 'Broadcast',
                      '11': 'Group addressing'}

APL_FRAGMENTATION = {'00': 'Transmission is not fragmented',
                     '01': 'Frame is first fragment',
                     '10': 'Frame is part of fragmented transmission',
                     '11': 'Reserved'}

AUX_SECURITY_LEVELS = {0: 'None',
                       1: 'MIC-32',
                       2: 'MIC-64',
                       3: 'MIC-128',
                       4: 'ENC',
                       5: 'ENC-MIC-32',
                       6: 'ENC-MIC-64',
                       7: 'ENC-MIC-128'}

AUX_KEY_IDENTIFIERS = {0: 'Data key',
                       1: 'Network key',
                       2: 'Key-transport key',
                       3: 'Key-load key'}


class INTERPRETER():

    def __init__(self):
        self.mac_dict = {}
        self.nwk_dict = {}
        self.apl_dict = {}
        self.nwk_aux_dict = {}
        self.apl_aux_dict = {}
        self.general_dict = {}
        self.enc_data = ""

        self.bitArray = []
        self.bitString = ''

    def to_bits(self, data=''):
        scale = 16
        num_of_bits = 4
        self.bitArray = []
        self.bitString = ''

        for h in hexlify(data):
            # Convert to binary
            h = bin(int(h, scale))[2:].zfill(num_of_bits)
            # Add converted data to array
            self.bitArray.append(h)

        return self.bitString.join(self.bitArray)

    def bits_to_hex(self, binarystring=''):
        counter = 0
        hexstring_list = []
        while counter < len(binarystring):
            tmp_string = binarystring[counter:counter + 8]
            hexstring_list.append(format(int(tmp_string, 2), 'x'))
            counter += 8
        hexstring = ''.join(hexstring_list)
        return hexstring

    def get_address(self, data=''):
        byte_buffer = []
        address = ':'
        i = 0

        while i < len(data):
            addr = data[i:i + 8]
            addr = format(int(addr, 2), 'x').zfill(2)
            byte_buffer.append(addr)
            i = i + 8

        byte_buffer = byte_buffer[::-1]
        address = address.join(byte_buffer)
        return address

    def parse_mac(self, cur_pos=0):
        # Helper variables
        pan_comp = 0
        dest_mode = 0
        source_mode = 0
        souce_addr_span = 0
        dest_addr_span = 0
        pan_addr_span = 16

        # 1st Byte Frame Control
        # self.mac_dict['Reserved'] = int(self.bitString[cur_pos:cur_pos+1], 2)
        cur_pos += 1
        self.mac_dict['pan_id_comp'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
        pan_comp = int(self.bitString[cur_pos:cur_pos + 1], 2)
        cur_pos += 1
        self.mac_dict['ack_req'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
        cur_pos += 1
        self.mac_dict['frame_pending'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
        cur_pos += 1
        self.mac_dict['security_enabled'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
        cur_pos += 1
        self.mac_dict['frame_type'] = MAC_FRAME_TYPES[int(self.bitString[cur_pos:cur_pos + 3], 2)]
        cur_pos += 3

        # 2nd Byte Frame Control
        self.mac_dict['src_addr_mode'] = MAC_ADDRESSING_MODES[int(self.bitString[cur_pos:cur_pos + 2], 2)]
        source_mode = int(self.bitString[cur_pos:cur_pos + 2], 2)
        cur_pos += 2
        self.mac_dict['frame_version'] = int(self.bitString[cur_pos:cur_pos + 2], 2)
        cur_pos += 2
        self.mac_dict['dst_addr_mode'] = MAC_ADDRESSING_MODES[int(self.bitString[cur_pos:cur_pos + 2], 2)]
        dest_mode = int(self.bitString[cur_pos:cur_pos + 2], 2)
        cur_pos += 2
        # self.mac_dict['Reserved'] = int(self.bitString[cur_pos:cur_pos+2], 2)
        cur_pos += 2

        # 1 Byte sequence_number
        self.mac_dict['sequence_number'] = int(self.bitString[cur_pos:cur_pos + 8], 2)
        cur_pos += 8

        if dest_mode == 2:
            dest_addr_span = 16
        if dest_mode == 3:
            dest_addr_span = 64
        if source_mode == 2:
            souce_addr_span = 16
        if source_mode == 3:
            souce_addr_span = 64

        # Only calculate dst_addres if addressing mode is not "None" or "Reserved"
        if dest_mode == 2 or dest_mode == 3:

            # Set dest. PAN 1D
            self.mac_dict['dst_pan_id'] = self.get_address(self.bitString[cur_pos:cur_pos + pan_addr_span])
            cur_pos += pan_addr_span

            # Set IEEE dst_addr
            self.mac_dict['dst_addr'] = self.get_address(self.bitString[cur_pos:cur_pos + dest_addr_span])
            cur_pos += dest_addr_span
        # If Dst. Mode == Reserved return -1
        elif dest_mode == 1:
            return -1

        # Only calculate src_addres if addressing mode is not "None" or "Reserved"
        if source_mode == 2 or source_mode == 3:

            # Set src_pan_id
            if pan_comp == 1:
                self.mac_dict['src_pan_id'] = self.mac_dict['dst_pan_id']
            else:
                self.mac_dict['src_pan_id'] = self.get_address(self.bitString[cur_pos:cur_pos + pan_addr_span])
                cur_pos += pan_addr_span

            # Set IEEE src_addr
            self.mac_dict['src_addr'] = self.get_address(self.bitString[cur_pos:cur_pos + souce_addr_span])
            cur_pos += souce_addr_span
        # If Src. Mode == Reserved return -1
        elif source_mode == 1:
            return -1

        # If MAC frame type is Beacon or Reserved, return -1, otherwise return current prosition in bitstring
        # and continue processing
        if self.mac_dict.get('frame_type') == 'Beacon' or self.mac_dict.get('frame_type') == 'Reserved':
            return -1
        else:
            return cur_pos

    def parse_nwk(self, cur_pos=0):
        if self.mac_dict.get('frame_type') == 'ACK':
            return -1

        # 1st Byte frame control
        self.nwk_dict['discover_route'] = DISCOVER_ROUTES[int(self.bitString[cur_pos:cur_pos + 2], 2)]
        cur_pos += 2
        self.nwk_dict['protocol_version'] = NWK_PROTOCOL_VERSIONS[int(self.bitString[cur_pos:cur_pos + 4], 2)]
        cur_pos += 4
        self.nwk_dict['frame_type'] = NWK_FRAME_TYPES[self.bitString[cur_pos:cur_pos + 2]]
        cur_pos += 2

        # 2nd Byte frame control
        # self.nwk_dict['Reserved'] = int(self.bitString[cur_pos:cur_pos+2], 2)
        cur_pos += 2
        self.nwk_dict['end_device_initiator'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
        cur_pos += 1
        self.nwk_dict['src_ieee_addr_present'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
        cur_pos += 1
        self.nwk_dict['dst_ieee_addr_present'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
        cur_pos += 1
        self.nwk_dict['source_route'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
        cur_pos += 1
        self.nwk_dict['security'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
        cur_pos += 1
        self.nwk_dict['multicast'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
        cur_pos += 1

        # 2 Byte Destination address
        self.nwk_dict['dst_addr'] = self.get_address(self.bitString[cur_pos:cur_pos + 16])
        cur_pos += 16

        # 2 Byte src_addr
        self.nwk_dict['src_addr'] = self.get_address(self.bitString[cur_pos:cur_pos + 16])
        cur_pos += 16

        # 1 Byte radius
        self.nwk_dict['radius'] = int(self.bitString[cur_pos:cur_pos + 8], 2)
        cur_pos += 8

        # 1 Byte sequence_number
        self.nwk_dict['sequence_number'] = int(self.bitString[cur_pos:cur_pos + 8], 2)
        cur_pos += 8

        # 8 Byte dst_ieee_addr
        if self.nwk_dict.get('dst_ieee_addr_present'):
            self.nwk_dict['dst_ieee_addr'] = self.get_address(self.bitString[cur_pos:cur_pos + 64])
            cur_pos += 64

        # 8 Byte src_ieee_addr
        if self.nwk_dict.get('src_ieee_addr_present'):
            self.nwk_dict['src_ieee_addr'] = self.get_address(self.bitString[cur_pos:cur_pos + 64])
            cur_pos += 64

        # 1 Byte multicast control
        if self.nwk_dict.get('multicast'):
            self.nwk_dict['max_nonmember_radius'] = int(self.bitString[cur_pos:cur_pos + 3], 2)
            cur_pos += 3
            self.nwk_dict['nonmember_radius'] = int(self.bitString[cur_pos:cur_pos + 3], 2)
            cur_pos += 3
            self.nwk_dict['multicast_mode'] = MULTICAST_MODES[int(self.bitString[cur_pos:cur_pos + 2], 2)]
            cur_pos += 2

        # n Byte souce route subframe
        if self.nwk_dict.get('source_route'):
            self.nwk_dict['relay_count'] = int(self.bitString[cur_pos:cur_pos + 8], 2)
            cur_pos += 8
            self.nwk_dict['relay_index'] = int(self.bitString[cur_pos:cur_pos + 8], 2)
            cur_pos += 8
            # Add a list of relays
            self.nwk_dict['relay_list'] = []
            for i in range(self.nwk_dict.get('relay_count')):
                self.nwk_dict['relay_list'].append(self.get_address(self.bitString[cur_pos:cur_pos + 16]))
                cur_pos += 16

        return cur_pos

    def parse_nwk_aux(self, cur_pos=0):
        if self.nwk_dict.get('security'):
            # 1 Byte security control
            # self.nwk_aux_dict['Reserved'] = int(self.bitString[cur_pos:cur_pos+2], 2)
            cur_pos += 2
            self.nwk_aux_dict['extended_nonce'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
            cur_pos += 1
            self.nwk_aux_dict['key_identifier'] = AUX_KEY_IDENTIFIERS[int(self.bitString[cur_pos:cur_pos + 2], 2)]
            cur_pos += 2
            self.nwk_aux_dict['security_lvl'] = AUX_SECURITY_LEVELS[int(self.bitString[cur_pos:cur_pos + 3], 2)]
            cur_pos += 3

            # 4 Byte frame_counter
            self.nwk_aux_dict['frame_counter'] = int(self.bitString[cur_pos:cur_pos + 32], 2)
            cur_pos += 32

            # 8 Byte src_addr
            if self.nwk_aux_dict.get('extended_nonce'):
                self.nwk_aux_dict['src_addr'] = self.get_address(self.bitString[cur_pos:cur_pos + 64])
                cur_pos += 64

            # 1 Byte key_sequence_number
            if self.nwk_aux_dict.get('key_identifier') == 'Network key':
                self.nwk_aux_dict['key_sequence_number'] = int(self.bitString[cur_pos:cur_pos + 8], 2)
                cur_pos += 8

            # 1 Byte NWK command identifier
            if self.nwk_dict.get('frame_type') == 'NWK command':
                if self.nwk_aux_dict.get('security_lvl') != AUX_SECURITY_LEVELS.get(4) and \
                        self.nwk_aux_dict.get('security_lvl') != AUX_SECURITY_LEVELS.get(5) and \
                        self.nwk_aux_dict.get('security_lvl') != AUX_SECURITY_LEVELS.get(6) and \
                        self.nwk_aux_dict.get('security_lvl') != AUX_SECURITY_LEVELS.get(7):
                    self.nwk_dict['command_id'] = NWK_COMMAND_ID.get(int(self.bitString[cur_pos:cur_pos + 8], 2))
                    cur_pos += 8

            return cur_pos

    def parse_apl(self, cur_pos=0):
        if self.nwk_dict.get('security'):
            self.enc_data = self.bits_to_hex(self.bitString[cur_pos:])
            return -1
        if self.nwk_dict.get('frame_type') == 'NWK command' or self.mac_dict.get('frame_type') == 'ACK':
            return -1

        # 1 Byte frame control
        self.apl_dict['extended_header_present'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
        cur_pos += 1
        self.apl_dict['ack_req'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
        cur_pos += 1
        self.apl_dict['security'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
        cur_pos += 1
        self.apl_dict['ack_format'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
        cur_pos += 1
        self.apl_dict['delivery_mode'] = APL_DELIVERY_MODES[self.bitString[cur_pos:cur_pos + 2]]
        cur_pos += 2
        self.apl_dict['frame_type'] = APL_FRAME_TYPES[self.bitString[cur_pos:cur_pos + 2]]
        cur_pos += 2

        # 1 Byte dst_endpoint -- Only present if normal unicast or broadcast
        if self.apl_dict.get('delivery_mode') == 'Normal unicast' or self.apl_dict.get('delivery_mode') == 'Broadcast':
            if not (self.apl_dict.get('ack_format') and self.apl_dict.get('frame_type') == 'ACK'):
                self.apl_dict['dst_endpoint'] = int(self.bitString[cur_pos:cur_pos + 8], 2)
                cur_pos += 8

        # 2 Byte group_addr -- Only present if group addressing
        if self.apl_dict.get('delivery_mode') == 'Group addressing':
            self.apl_dict['group_addr'] = self.get_address(self.bitString[cur_pos:cur_pos + 16])
            cur_pos += 16

        # 2 Byte cluster_id -- Only present for data and ack frames
        if self.apl_dict.get('frame_type') == 'Data' or self.apl_dict.get('frame_type') == 'ACK':
            if (self.apl_dict.get('ack_format') and self.apl_dict.get('frame_type') == 'ACK') or self.apl_dict.get('frame_type') == 'Data':
                self.apl_dict['cluster_id'] = int(self.bitString[cur_pos:cur_pos + 16], 2)
                cur_pos += 16

        # 2 Byte profile_id -- Only present for data and ack frames
        if self.apl_dict.get('frame_type') == 'Data' or self.apl_dict.get('frame_type') == 'ACK':
            if (self.apl_dict.get('ack_format') and self.apl_dict.get('frame_type') == 'ACK') or self.apl_dict.get('frame_type') == 'Data':
                self.apl_dict['profile_id'] = int(self.bitString[cur_pos:cur_pos + 16], 2)
                cur_pos += 16

        # 1 Byte src_endpoint
        if self.apl_dict.get('frame_type') == 'Data' or self.apl_dict.get('frame_type') == 'ACK':
            if not (self.apl_dict.get('ack_format') and self.apl_dict.get('frame_type') == 'ACK'):
                self.apl_dict['src_endpoint'] = int(self.bitString[cur_pos:cur_pos + 8], 2)
                cur_pos += 8

        # 1 Byte aps_counter
        self.apl_dict['aps_counter'] = int(self.bitString[cur_pos:cur_pos + 8], 2)
        cur_pos += 8

        if self.apl_dict.get('extended_header_present'):
            # Reserved
            cur_pos += 6
            # 1 Bit fragmentation
            self.apl_dict['fragmentation'] = APL_FRAGMENTATION[self.bitString[cur_pos:cur_pos + 2]]
            cur_pos += 2

            # 1 Byte block_number
            if self.apl_dict.get('fragmentation') == 'Frame is first fragment' or self.apl_dict.get('fragmentation') == 'Frame is part of fragmented transmission':
                self.apl_dict['block_number'] = int(self.bitString[cur_pos:cur_pos + 8], 2)
                cur_pos += 8

            # 1 Byte ack_bitfield
            if self.apl_dict.get('frame_type') == 'ACK' and (self.apl_dict.get('fragmentation') == 'Frame is first fragment' or self.apl_dict.get('fragmentation') == 'Frame is part of fragmented transmission'):
                self.apl_dict['ack_bitfield'] = int(self.bitString[cur_pos:cur_pos + 8], 2)
                cur_pos += 8

        return cur_pos

    def parse_apl_aux(self, cur_pos=0):
        # Only process if aplication security is true
        if self.apl_dict.get('security'):
            # 1 Byte security control
            # self.aux_dict['Reserved'] = int(self.bitString[cur_pos:cur_pos+2], 2)
            cur_pos += 2
            self.apl_aux_dict['extended_nonce'] = BOOL_VALUES[int(self.bitString[cur_pos:cur_pos + 1], 2)]
            cur_pos += 1
            self.apl_aux_dict['key_identifier'] = AUX_KEY_IDENTIFIERS[int(self.bitString[cur_pos:cur_pos + 2], 2)]
            cur_pos += 2
            self.apl_aux_dict['security_lvl'] = AUX_SECURITY_LEVELS[int(self.bitString[cur_pos:cur_pos + 3], 2)]
            cur_pos += 3

            # 4 Byte frame_counter
            self.apl_aux_dict['frame_counter'] = int(self.bitString[cur_pos:cur_pos + 32], 2)
            cur_pos += 32

            # 8 Byte src_addr
            if self.apl_aux_dict.get('extended_nonce'):
                self.apl_aux_dict['src_addr'] = self.get_address(self.bitString[cur_pos:cur_pos + 64])
                cur_pos += 64

            # 1 Byte key_sequence_number
            if self.apl_aux_dict.get('key_identifier') == 'Network key':
                self.apl_aux_dict['key_sequence_number'] = int(self.bitString[cur_pos:cur_pos + 8], 2)
                cur_pos += 8

        return cur_pos

    def parse(self, data=''):
        # Clear frame dict bevore parsing
        self.general_dict.clear()
        self.mac_dict.clear()
        self.nwk_dict.clear()
        self.apl_dict.clear()
        self.nwk_aux_dict.clear()
        self.apl_aux_dict.clear()

        # Convert binary data to bit string
        self.bitString = self.to_bits(data)

        cur_pos = 0

        if self.bitString[cur_pos:]:
            cur_pos = self.parse_mac(cur_pos)

        if cur_pos >= 0:
            if self.bitString[cur_pos:]:
                cur_pos = self.parse_nwk(cur_pos)

        if cur_pos >= 0:
            if self.bitString[cur_pos:]:
                cur_pos = self.parse_nwk_aux(cur_pos)

        if cur_pos >= 0:
            if self.bitString[cur_pos:]:
                cur_pos = self.parse_apl(cur_pos)

        if cur_pos >= 0:
            if self.bitString[cur_pos:]:
                cur_pos = self.parse_apl_aux(cur_pos)

    def show_Frame(self):
        msg = '############### NEW FRAME ###############\n'

        if self.mac_dict:
            msg += '\n[MAC HEADER]\n'
            for key in self.mac_dict:
                msg += '  %s = %s\n' % (key, self.mac_dict[key])

        if self.nwk_dict:
            msg += '\n[NETWORK HEADER]\n'
            for key in self.nwk_dict:
                msg += '  %s = %s\n' % (key, self.nwk_dict[key])

        if self.nwk_aux_dict:
            msg += '\n[NWK AUXILIARY HEADER]\n'
            for key in self.nwk_aux_dict:
                msg += '  %s = %s\n' % (key, self.nwk_aux_dict[key])

        if self.apl_dict:
            msg += '\n[APPLICATION HEADER]\n'
            for key in self.apl_dict:
                msg += '  %s = %s\n' % (key, self.apl_dict[key])

        if self.apl_aux_dict:
            msg += '\n[APL AUXILIARY HEADER]\n'
            for key in self.apl_aux_dict:
                msg += '  %s = %s\n' % (key, self.apl_aux_dict[key])

        print(msg)
