ZigScanner - A warwalking tool using CC2531 dongles
===

ZigScanner is based on mitshell's [IEEE 802.15.4 monitor](https://github.com/mitshell/CC2531) and is using its CC2531 class. ZigScanner can drive up to 16 CC2531 Dongles simultaneously. Collected information gets dumped into an "output.csv" file. This file contains a timestamp, positional data (if GPS is connected) and the raw zigbee frame inlcuding mac header. To parse the raw frames into something more readable, a "parser.py" utility is included.

## Dependencies

libusb1

	sudo apt-get install libusb-1.0-0-dev

python-libusb1 - [https://github.com/vpelletier/python-libusb1](https://github.com/vpelletier/python-libusb1)

	pip install libusb1

pySerial - [https://github.com/pyserial/pyserial](https://github.com/pyserial/pyserial)

	pip install pyserial

libmich library - [https://github.com/mitshell/libmich](https://github.com/mitshell/libmich)

	git clone https://github.com/mitshell/libmich.git
	cd libmich
	python setup.py install

## Usage

Call "./sniffer.py -h" for usage information.

	-h, --help			show this help message and exit
 	-s, --silent		run in silent mode and don't write packet information
						to stdout
  	-v {0,1,2,3}, --verbose {0,1,2,3}
						sets verbosity level to output additional debug
						information (0 = none, 3 = max, default = 0)
  	-p, --position		print positional data to stdout
  	-c {11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}, --channel {11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}
						define a single channel to scan


Note that this program has to be run with root priviledges, otherwise it can't access and configure connected CC2531 dongles.

Collected data gets dumped into a "output.csv" file, which contains the raw zigbee frame, a timestamp and positional data. To parse the output.csv into a more human-readable format, execute the included "parser.py" file.

	./parser.py -i path/to/output.csv

This will create a "parsed-output.csv" and saves it in the current working directory.

Happy scanning!
