#!/usr/bin/env python

import argparse
import csv
import os
import sys
from binascii import unhexlify
from modules.Interpreter import INTERPRETER

interpreter = INTERPRETER()


def convert_oui(address):
    ieee_address = str(address).replace(':', '')
    ieee_address = ieee_address[:6]
    with open('oui/oui.csv', 'r') as input_file:
        csv_reader = csv.DictReader(input_file)
        for line in csv_reader:
            if str(line['mac']).lower() == str(ieee_address).lower():
                return line['vendor']


def create_oui_csv(oui_file):
    with open(oui_file, 'r') as in_file:
        with open('oui/oui.csv', 'w') as out_file:

            fieldnames = ['mac', 'vendor']
            data_dict = {'mac': '', 'vendor': ''}

            csv_writer = csv.DictWriter(out_file, fieldnames=fieldnames)
            csv_writer.writeheader()

            for line in in_file:
                line = line.replace('\t', ' ').replace('\n', '').replace('\r', '')
                line = line.split(' ')
                if len(line[0]) == 6:
                    data_dict['mac'] = line[0]
                    data_dict['vendor'] = str(' '.join(line[8:]))
                    csv_writer.writerow(data_dict)


def get_decimal_degrees_longitude(longitude):
    if longitude:
        longitude = str(longitude)
        degrees = float(longitude[0:3])
        seconds = float(float(longitude[3:]) / 60)
        return "{0:.6f}".format(degrees + seconds)
    else:
        return ""


def get_decimal_degrees_latitude(latitude):
    if latitude:
        latitude = str(latitude)
        degrees = float(latitude[0:2])
        seconds = float(float(latitude[2:]) / 60)
        return "{0:.6f}".format(degrees + seconds)
    else:
        return ""


def parse_output(parserInput):
    parserOutput = "output/" + "parsed_" + os.path.basename(parserInput)
    parserRoute = "output/" + "route_" + os.path.basename(parserInput)

    with open(parserInput, 'r') as input_file, open(parserOutput, 'w') as output_file, open(parserRoute, 'w') as routes_file:
        csv_reader = csv.DictReader(input_file)

        routes_fieldnames = ['title',
                             'lat',
                             'lon']

        output_fieldnames = ['time',
                             'nmea_longitude',
                             'decimal_degrees_longitude',
                             'nmea_latitude',
                             'decimal_degrees_latitude',
                             'vendor',
                             'mac_frame_type',
                             'mac_security',
                             'mac_dest_pan_id',
                             'mac_dest_addr',
                             'mac_src_pan_id',
                             'mac_src_addr',
                             'nwk_frame_type',
                             'nwk_protocol_version',
                             'nwk_security',
                             'nwk_dst_addr',
                             'nwk_src_addr',
                             'nwk_dst_ieee',
                             'nwk_src_ieee',
                             'nwk_security_lvl',
                             'nwk_key_identifier',
                             'nwk_aux_src_addr',
                             'nwk_key_sequence_nr',
                             'apl_frame_type',
                             'apl_delivery_mode',
                             'apl_security',
                             'apl_dest_endpoint',
                             'apl_group_addr',
                             'apl_cluster_id',
                             'apl_security_lvl',
                             'apl_key_identifier',
                             'apl_aux_src_addr',
                             'apl_key_sequence_nr',
                             'further_data']

        parsed_data = {}
        csv_writer = csv.DictWriter(output_file, fieldnames=output_fieldnames)
        csv_writer.writeheader()

        routes_data = {}
        csv_routes_writer = csv.DictWriter(routes_file, fieldnames=routes_fieldnames)
        csv_routes_writer.writeheader()
        # Title for each point in routes file
        routes_title = 1
        mac_dest_pan_list = []

        # Line Nr. for debug information
        line_nr = 1
        for line in csv_reader:
            oui_set = False

            try:
                rawData = unhexlify(line['rawData'])
            except Exception as e:
                print('Error unhexlifying frame: %s' % e)
                line_nr += 1

            try:
                interpreter.parse(rawData)
            except Exception as e:
                print('Error parsing frame nr. %s: %s' % (line_nr, line['rawData']))
                line_nr += 1

            line_nr += 1

            try:
                vendor = ''
                if interpreter.mac_dict.get('src_addr'):
                    if len(interpreter.mac_dict.get('src_addr')) > 6:
                        vendor = convert_oui(interpreter.mac_dict.get('src_addr'))
                        oui_set = True

                if interpreter.nwk_dict.get('src_ieee_addr') and not oui_set:
                    vendor = convert_oui(interpreter.nwk_dict.get('src_ieee_addr'))
                    oui_set = True

                if interpreter.nwk_aux_dict.get('src_addr') and not oui_set:
                    vendor = convert_oui(interpreter.nwk_aux_dict.get('src_addr'))
                    oui_set = True

                if interpreter.apl_aux_dict.get('src_addr') and not oui_set:
                    vendor = convert_oui(interpreter.apl_aux_dict.get('src_addr'))
            except Exception as e:
                print('Error parsing vendor: %s' % e)

            # Routes file filler
            if line.get('latitude') and line.get('longitude'):
                if not interpreter.mac_dict.get('dst_pan_id') in mac_dest_pan_list:
                    routes_data['title'] = routes_title
                    routes_data['lat'] = get_decimal_degrees_latitude(line.get('latitude'))
                    routes_data['lon'] = get_decimal_degrees_longitude(line.get('longitude'))
                    routes_title += 1
                    mac_dest_pan_list.append(interpreter.mac_dict.get('dst_pan_id'))

                    csv_routes_writer.writerow(routes_data)
                    routes_data.clear()

            parsed_data['time'] = line.get('time')
            parsed_data['nmea_longitude'] = line.get('longitude')
            parsed_data['decimal_degrees_longitude'] = get_decimal_degrees_longitude(line.get('longitude'))
            parsed_data['nmea_latitude'] = line.get('latitude')
            parsed_data['decimal_degrees_latitude'] = get_decimal_degrees_latitude(line.get('latitude'))
            parsed_data['vendor'] = vendor
            parsed_data['mac_frame_type'] = interpreter.mac_dict.get('frame_type')
            parsed_data['mac_security'] = interpreter.mac_dict.get('security_enabled')
            parsed_data['mac_dest_pan_id'] = interpreter.mac_dict.get('dst_pan_id')
            parsed_data['mac_dest_addr'] = interpreter.mac_dict.get('dst_addr')
            parsed_data['mac_src_pan_id'] = interpreter.mac_dict.get('src_pan_id')
            parsed_data['mac_src_addr'] = interpreter.mac_dict.get('src_addr')
            parsed_data['nwk_frame_type'] = interpreter.nwk_dict.get('frame_type')
            parsed_data['nwk_protocol_version'] = interpreter.nwk_dict.get('protocol_version')
            parsed_data['nwk_security'] = interpreter.nwk_dict.get('security')
            parsed_data['nwk_dst_addr'] = interpreter.nwk_dict.get('dst_addr')
            parsed_data['nwk_src_addr'] = interpreter.nwk_dict.get('src_addr')
            parsed_data['nwk_dst_ieee'] = interpreter.nwk_dict.get('dst_ieee_addr')
            parsed_data['nwk_src_ieee'] = interpreter.nwk_dict.get('src_ieee_addr')
            parsed_data['nwk_security_lvl'] = interpreter.nwk_aux_dict.get('security_lvl')
            parsed_data['nwk_key_identifier'] = interpreter.nwk_aux_dict.get('key_identifier')
            parsed_data['nwk_aux_src_addr'] = interpreter.nwk_aux_dict.get('src_addr')
            parsed_data['nwk_key_sequence_nr'] = interpreter.nwk_aux_dict.get('key_sequence_number')
            parsed_data['apl_frame_type'] = interpreter.apl_dict.get('frame_type')
            parsed_data['apl_delivery_mode'] = interpreter.apl_dict.get('delivery_mode')
            parsed_data['apl_security'] = interpreter.apl_dict.get('security')
            parsed_data['apl_dest_endpoint'] = interpreter.apl_dict.get('dst_endpoint')
            parsed_data['apl_group_addr'] = interpreter.apl_dict.get('group_addr')
            parsed_data['apl_cluster_id'] = interpreter.apl_dict.get('cluster_id')
            parsed_data['apl_security_lvl'] = interpreter.apl_aux_dict.get('security_lvl')
            parsed_data['apl_key_identifier'] = interpreter.apl_aux_dict.get('key_identifier')
            parsed_data['apl_aux_src_addr'] = interpreter.apl_aux_dict.get('src_addr')
            parsed_data['apl_key_sequence_nr'] = interpreter.apl_aux_dict.get('key_sequence_number')
            parsed_data['further_data'] = interpreter.enc_data

            csv_writer.writerow(parsed_data)

            parsed_data.clear()


def main():
    parser = argparse.ArgumentParser(description='Takes output file of sniffer.py as input and parses ZigBee communication')
    parser.add_argument('-i', '--input', type=str, default=None, help='file containing output of sniffer.py')
    # argcomplete.autocomplete(parser)
    args = parser.parse_args()

    if not os.path.exists('oui/oui.txt'):
        print('[Error] oui.txt file not found.\n')
        print('[Info] Please download oui.txt and place it inside the zigscanner dirctory.\nDownload link: http://standards-oui.ieee.org/oui.txt')
        sys.exit(0)

    if not os.path.exists('oui/oui.csv'):
        create_oui_csv('oui/oui.txt')

    if not args.input:
        print('[Error] Please specify an input file.')
        sys.exit(0)

    if os.path.exists(args.input):
        parse_output(args.input)
    else:
        print('[Info] Input file not found.')
        sys.exit(0)


if __name__ == '__main__':
    main()
